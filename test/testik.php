<?php
/**
 * Created by PhpStorm.
 * User: xfoldvar
 * Date: 4/19/2018
 * Time: 5:51 PM
 */

print_r(__DIR__ . "\\..\\examples\\Converter.php");

require_once(__DIR__ . "\\..\\examples\\Converter.php");



function printPrice($currency, $price){
    echo "\nYour crypto price in " . $currency . " is: " . $price . "\n";
}

$converter = new Converter();
$moneroPrice = $converter->convertUsdPrice(Converter::XMR,33);
printPrice(Converter::XMR, $moneroPrice);
