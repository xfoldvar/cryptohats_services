<?php
require_once(__DIR__ . "/../vendor/autoload.php");
use coinmarketcap\api\CoinMarketCap;

/**
 * Created by PhpStorm.
 * User: xfoldvar
 * Date: 4/19/2018
 * Time: 5:50 PM
 */

class Converter {

    const XMR = "monero",
        BTC = "bitcoin",
        LTC = "litecoin";

    /**
     * Vrati cenu v kryptomene
     */
    public function convertUsdPrice($currency, $value){
        $apiResult = CoinMarketCap::getCurrencyTicker($currency, "USD");
        $apiResult = array_pop($apiResult);
        $result = $value / $apiResult["price_usd"];
        return round($result, 4, 0);
    }


}